<?php

class IndexController extends System\MyController
{
    public function indexAction()
    {
        $this->redirect(url."/sessao/entrar");
//        $path = "/home/fernando/Dropbox/workspace/workspace-php/HazoNote/media/sistema2/cadernos/2014/04/08";
        
//        mkdir($path, 0777, true);
    }
    
    public function criarUsuarioAction($email=null)
    {
        echo "CRIAR usuário: <br/><br/>";
//        $Usuario = new Model\Usuario($email);
        
        $Usuario = new Model\Usuario($email);
        $Usuario->setSenha("teste");
        $Usuario->setNome("Fernando Gurkievicz");
        
        $Usuario->save();
        
    }
    
    public function getUsuarioAction($email=null)
    {
        echo "GET usuário: <br/><br/>";
//        $Usuario = new Model\Usuario($email);
        
        $Usuario = new Model\Usuario($email);
        debug($Usuario);
        
    }
    
    public function deleteUsuarioAction($email=null)
    {
        echo "DELETE usuário: <br/><br/>";
//        $Usuario = new Model\Usuario($email);
        
        $Usuario = new Model\Usuario($email);
        debug($Usuario->delete());
        
    }
    
    public function visualizarCadernoAction( $token )
    {
        $Caderno = new Model\Caderno($token);
        debug($Caderno);
    }
    
    public function criarCadernoAction()
    {
        $Caderno = new Model\Caderno();
        $Caderno->setTitulo('Caderno x001');
        $Caderno->save();
        debug( $Caderno );
    }
    
    public function excluirCadernoAction( $token )
    {
        $Caderno = new Model\Caderno($token);
        $Caderno->delete();
    }
    
}