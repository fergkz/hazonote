<?php

use Model\Usuario as Usuario;

class SessaoController extends System\MyController
{
    public function indexAction()
    {
        $this->redirect(url."/sessao/entrar");
    }
    
    public function entrarAction()
    {
        if( Usuario::getOnline() ){
            $this->redirect(url."/painel");
        }
        
        if( $_POST ){
            if( Usuario::login($_POST['email'], $_POST['senha']) ){
                $this->redirect(url."/painel");
            }else{
                ?>
                <p style="color:red;"><?=implode("<br/>",_getErrors());?></p>
                <?
                _clearErrors();
            }
        }
        ?>
        <form method="post" action="">
            <input type="text" name="email" placeholder="E-mail"/>
            <input type="text" name="senha" placeholder="Senha"/>
            <input type="submit" value="Entar"/>
        </form>
        <hr/>
        <a href="<?=url?>/sessao/cadastrar">Cadastrar</a>
        <?
    }
    
    public function sairAction()
    {
        Usuario::logout();
        $this->redirect(url."/sessao/entrar");
    }
    
    public function cadastrarAction()
    {
        if( $_POST ){
            $Usuario = new Usuario($_POST['email']);
            
            if( $Usuario->existe() ){
                _setError("Usuário já cadastrado no sistema");
            }else{
                $Usuario->setNome($_POST['nome']);
                $Usuario->setSenha($_POST['senha']);
                if( $Usuario->save() ){
                    $this->redirect(url."/painel");
                }
            }
            
            if(_getErrors()){
                ?><p style="color:red;"><?=implode("<br/>", _getErrors());?></p><?
            }
            _clearErrors();
        }
        ?>
        <form method="post" action="">
            <input type="text" name="email" placeholder="E-mail"/>
            <input type="text" name="senha" placeholder="Senha"/>
            <input type="text" name="nome" placeholder="Nome"/>
            <input type="submit" value="Entar"/>
        </form>
        <hr/>
        <a href="<?=url?>/sessao/entrar">Entrar</a>
        <?
    }
    
}