<?php

namespace Model;

use MVC\Session as Session;

class Usuario
{
    private $email;
    private $senha;
    private $nome;
    private $existe = false;
    
    private $cadernos = null;
    private $filename = null;
    
    public function setSenha($senha){ $this->senha = md5($senha); }
    public function getSenha(){ return $this->senha; }
    public function setNome($nome){ $this->nome = $nome; }
    public function getNome(){ return $this->nome; }
    public function getEmail(){ return $this->email; }
    public function existe(){ return $this->existe; }
    
    public function __construct( $email )
    {
        $this->email = trim($email);
        $this->setFilename($email);
        $this->load();
    }
    
    public function save()
    {
        $this->setFilename($this->email);

        $json = new \stdClass();

        $json->email    = $this->email;
        $json->senha    = $this->senha;
        $json->nome     = $this->nome;
        $json->cadernos = $this->cadernos;

        if( !file_exists(path.ds.'media'.ds.'sistema'.ds.'usuarios') ){
            mkdir(path.ds.'media'.ds.'sistema'.ds.'usuarios', 0777, true);
        }
        
        file_put_contents($this->filename, json_encode($json));
        chmod($this->filename, 0777);

//        debug($this);
        return true;
    }
    
    public function delete()
    {
        unlink($this->filename);
    }
    
    private function setFilename($email)
    {
        $this->filename = null;
        if( $email ){
            $this->filename = path.ds.'media'.ds.'sistema'.ds.'usuarios'.ds.$email.'.json';
        }
    }
    
    private function load()
    {
        if( $this->filename && file_exists($this->filename) ){
            $json = json_decode(file_get_contents($this->filename));
            $this->email    = $json->email;
            $this->senha    = $json->senha;
            $this->nome     = $json->nome;
            $this->existe   = true;
            $this->cadernos = (array)$json->cadernos;
        }
//        debug($this,1,'load');
    }
    
    public static function getOnline()
    {
        return (Session::get("usuario_online") ? new Usuario(Session::get("usuario_online")) : null);
    }
    
    public static function login( $email, $senha )
    {
        self::logout();
        $Usuario = new Usuario($email);
        if( $Usuario->existe() ){
            $senhaCript = md5($senha);
            if( $senhaCript !== $Usuario->getSenha() ){
                _setError("Senha ou e-mail inválido");
                return false;
            }else{
                Session::set("usuario_online", $Usuario->getEmail());
                return true;
            }
        }else{
            _setError("Senha ou e-mail inválido");
            return false;
        }
    }
    
    public static function logout()
    {
        Session::set("usuario_online", null);
    }
    
    public function listaCadernos()
    {
        if( !$this->cadernos ){
            return false;
        }else{
            foreach( $this->cadernos as $token ){
                $dados[] = new Caderno($token);
            }
            return $dados;
        }
    }
    
    public function addCaderno($token)
    {
        $this->cadernos[$token] = $token;
        $this->save();
    }
    
    public function delCaderno($token)
    {
        unset($this->cadernos[$token]);
        $this->save();
    }
}