<?php

namespace Model;

use Core\System\Functions as Functions;
use Model\Usuario as Usuario;
use Lib\File as File;

class Nota
{
    private $path;   #2014/04/08/12/12/12/fergkz@gmail.com
    private $token;  #path criptografado
    private $titulo;
    private $conteudo;
    private $dataCadastro = null;
    private $autor;
    private $cadernoToken;
    
    private $existe = false;
    
    private static $secretKey = "GRAVOWSKY";
    private static $secretLenght = 20;
    
    public function getToken(){ return $this->token; }
    public function getTitulo(){ return $this->titulo; }
    public function setTitulo( $titulo ){ $this->titulo = $titulo; }
    public function getConteudo(){ return $this->conteudo; }
    public function setConteudo( $conteudo ){ $this->conteudo = $conteudo; }
    public function getDataCadastro(){ return $this->dataCadastro; }
    public function existe(){ return $this->existe; }
    public function getCadernoToken(){ return $this->cadernoToken; }
    
    public function __construct( $notaToken = null, $cadernoToken = null )
    {
        if( $notaToken ){
            $this->token = $notaToken;
            $this->path = Functions::decrypt($notaToken, self::$secretKey, self::$secretLenght);
            $this->load();
        }else{
            $Caderno = new Caderno($cadernoToken);
            $this->cadernoToken = $Caderno->getToken();
        }
    }
    
    public function save()
    {
        $Usuario = Usuario::getOnline();
        
        if( !$Usuario || !is_object($Usuario) ){
            _setError("Somentem usuários online podem criar cadernos");
            return false;
        }

        if( !$this->autor ){
            $this->autor = $Usuario->getEmail();
        }
        
        $Caderno = new Caderno($this->cadernoToken);
        if( !in_array($Usuario->getEmail(),$Caderno->getUsuarios()) ){
            _setError("Caderno inacessível a este usuário");
            return false;
        }
        
        if( !$this->dataCadastro ){
            $this->dataCadastro = date("Y-m-d H:i:s");
            $this->path = $Caderno->getPath().ds.'notas'.ds.str_replace(array('-',':',' '), "-", $this->dataCadastro);
            $this->token = Functions::crypt($this->path, self::$secretKey, self::$secretLenght);
        }
        
        $json = new \stdClass();
        $json->path         = $this->path;
        $json->token        = $this->token;
        $json->titulo       = $this->titulo;
        $json->conteudo     = $this->conteudo;
        $json->dataCadastro = $this->dataCadastro;
        $json->autor        = $this->autor;
        $json->cadernoToken = $this->cadernoToken;
        
        $path = path.ds.'media'.ds.'sistema'.ds.'cadernos'.ds.$this->path;
        $filename = $path.ds.'detalhes.json';
        
        if( !file_exists($path) ){
            mkdir($path, 0777, true);
        }
        
        file_put_contents($filename, json_encode($json));
        chmod($filename, 0777);
        
        $Caderno->addNota($this->token);
        
        return true;
    }
    
    private function load()
    {
        $filename = path.ds.'media'.ds.'sistema'.ds.'cadernos'.ds.$this->path.ds.'detalhes.json';
        
        if( is_file($filename) ){
            $content = file_get_contents($filename);
            $json = json_decode($content);
            
            $this->path         = $json->path;
            $this->token        = $json->token;
            $this->titulo       = $json->titulo;
            $this->conteudo     = $json->conteudo;
            $this->dataCadastro = $json->dataCadastro;
            $this->autor        = $json->autor;
            $this->cadernoToken = $json->cadernoToken;
            
            $this->existe = true;
        }
    }
    
    public function delete()
    {
        $Caderno = new Caderno($this->cadernoToken);
        $Caderno->delNota($this->token);
        
        $filename = path.ds.'media'.ds.'sistema'.ds.'cadernos'.ds.$this->path;
        File::delete($filename);
    }
    
    public function listaNotas()
    {
        return false;
    }
    
}