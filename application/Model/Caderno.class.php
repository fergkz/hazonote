<?php

namespace Model;

use Core\System\Functions as Functions;
use Model\Usuario as Usuario;
use Lib\File as File;

class Caderno
{
    private $path;   #2014/04/08/12/12/12/fergkz@gmail.com
    private $token;  #path criptografado
    private $titulo;
    private $dataCadastro = null;
    private $usuarios = array();
    
    private $notas;
    private $anexos;
    
    private static $secretKey = "GRAVOWSKY";
    private static $secretLenght = 20;
    
    public function getPath(){ return $this->path; }
    public function getToken(){ return $this->token; }
    public function getTitulo(){ return $this->titulo; }
    public function setTitulo( $titulo ){ $this->titulo = $titulo; }
    public function getDataCadastro(){ return $this->dataCadastro; }
    public function getUsuarios(){ return $this->usuarios; }
    
    public function __construct( $token = null )
    {
        if( $token ){
            $this->token = $token;
            $this->path = Functions::decrypt($token, self::$secretKey, self::$secretLenght);
            $this->load();
        }
    }
    
    public function save()
    {
        $Usuario = Usuario::getOnline();
        
        if( !$Usuario || !is_object($Usuario) ){
            _setError("Somentem usuários online podem criar cadernos");
            return false;
        }
        
        $this->usuarios[$Usuario->getEmail()] = $Usuario->getEmail();
        
        if( !$this->dataCadastro ){
            $this->dataCadastro = date("Y-m-d H:i:s");
            $this->path = str_replace(array('-',':',' '), "/", $this->dataCadastro)."/".$Usuario->getEmail();
            $this->token = Functions::crypt($this->path, self::$secretKey, self::$secretLenght);
        }
        
        $json = new \stdClass();
        $json->path         = $this->path;
        $json->token        = $this->token;
        $json->titulo       = $this->titulo;
        $json->dataCadastro = $this->dataCadastro;
        $json->usuarios     = $this->usuarios;
        $json->notas        = $this->notas;
        
        $path = path.ds.'media'.ds.'sistema'.ds.'cadernos'.ds.$this->path;
        $filename = $path.ds.'detalhes.json';
        
        if( !file_exists($path) ){
            mkdir($path, 0777, true);
        }
        
        file_put_contents($filename, json_encode($json));
        chmod($filename, 0777);
        
        $Usuario->addCaderno($this->token);
        
        return true;
    }
    
    private function load()
    {
        $filename = path.ds.'media'.ds.'sistema'.ds.'cadernos'.ds.$this->path.ds.'detalhes.json';
        
        if( is_file($filename) ){
            $content = file_get_contents($filename);
            $json = json_decode($content);
            
            $this->path         = $json->path;
            $this->token        = $json->token;
            $this->titulo       = $json->titulo;
            $this->dataCadastro = $json->dataCadastro;
            $this->usuarios     = @(array)$json->usuarios;
            $this->notas        = @(array)$json->notas;
        }
    }
    
    public function delete()
    {
        foreach( $this->usuarios as $email ){
            $Usuarios = new Usuario($email);
            $Usuarios->delCaderno($this->token);
        }
        
        $filename = path.ds.'media'.ds.'sistema'.ds.'cadernos'.ds.$this->path;
        File::delete($filename);
    }
    
    public function listaNotas()
    {
        if( $this->notas && is_array($this->notas) && count($this->notas) > 0 ){
            foreach( $this->notas as $token ){
                $dados[] = new Nota($token);
            }
            return $dados;
        }else{
            return false;
        }
    }
    
    public function addNota( $notaToken )
    {
        $this->notas[$notaToken] = $notaToken;
        $this->save();
    }
    
    public function delNota( $notaToken )
    {
        unset($this->notas[$notaToken]);
        $this->save();
    }
    
}