<?php

use Model\Usuario as Usuario;

if( !Usuario::getOnline() ){
    header("Location: ".url."/sessao/entrar");
    exit;
}