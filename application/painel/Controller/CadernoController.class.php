<?php

use Model\Usuario as Usuario;
use Model\Caderno as Caderno;

class CadernoController extends System\MyController
{
    private $Usuario = null;
    
    public function __construct()
    {
        parent::__construct();
        $this->Usuario = Usuario::getOnline();
    }
    
    public function visualizacaoAction( $token = null )
    {
        $Caderno = new Caderno($token);
        $notas = $Caderno->listaNotas();
        
        ?>
        Caderno: <b><?=$Caderno->getTitulo();?></b>
        <hr/>
        <?
        if( $notas ){
            foreach( $notas as $Nota ){
                ?>
                <div style="border:1px solid blue;padding:0 4px;margin:4px;line-height:1px;">
                    <h4><?=$Nota->getTitulo()?></h4>
                    <p><?=$Nota->getConteudo()?></p>
                    <p><a href="<?=url?>/painel/nota/excluir/<?=$Nota->getToken()?>">Excluir Nota</a></p>
                </div>
                <?
            }
        }
        ?>
        <hr/>
        <a href="<?=url?>/painel/nota/cadastro/<?=$Caderno->getToken()?>">Adicionar Nota</a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?=url?>/painel/caderno/cadastro/<?=$Caderno->getToken()?>">Alterar Caderno</a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?=url?>/painel">Voltar ao Painel</a>
        <?
    }
    
    public function cadastroAction( $token = null )
    {
        $Caderno = new Caderno($token);

        if( $_POST ){
            
            $Caderno->setTitulo($_POST['titulo']);
            
            if( $Caderno->save() ){
                $this->redirect(url."/painel/caderno/visualizacao/".$Caderno->getToken());
            }elseif(_getErrors()){
                ?><p style="color:red;"><?=implode("<br/>", _getErrors());?></p><?
            }
            _clearErrors();
        }
        ?>
        <form method="post" action="">
            <input type="text" name="titulo" placeholder="Título" value="<?=$Caderno->getTitulo();?>"/>
            <input type="submit" value="Enviar"/>
        </form>
        <?
    }
    
    
    ################### Funções definitivas
    
    
    public function listaCadernosAction()
    {
        $dados['status'] = false;
        $cadernos = Usuario::getOnline()->listaCadernos();
        if( $cadernos ){
            $dados['status'] = true;
            foreach( $cadernos as $Caderno ){
                $Obj = new stdClass();
                $Obj->token = $Caderno->getToken();
                $Obj->titulo = $Caderno->getTitulo();
                $dados['rows'][] = $Obj;
            }
        }
        $this->json($dados);
    }
    
    
}