<?php

use Model\Usuario as Usuario;
use Model\Nota as Nota;

class NotaController extends System\MyController
{
    private $Usuario = null;
    
    public function __construct()
    {
        parent::__construct();
        $this->Usuario = Usuario::getOnline();
    }
    
    public function cadastroAction( $cadernoToken, $notaToken = null )
    {
        $Nota = new Nota($notaToken, $notaToken ? null : $cadernoToken);
        
        if( $_POST ){

            $Nota->setTitulo($_POST['titulo']);
            $Nota->setConteudo($_POST['conteudo']);
            
            if( $Nota->save() ){
                $this->redirect(url."/painel/caderno/visualizacao/".$Nota->getCadernoToken());
            }elseif(_getErrors()){
                ?><p style="color:red;"><?=implode("<br/>", _getErrors());?></p><?
            }
            _clearErrors();
        }
        ?>
        <form method="post" action="">
            <input type="text" name="titulo" placeholder="Título" value="<?=$Nota->getTitulo();?>"/>
            <br/>
            <textarea name="conteudo" placeholder="Conteúdo"><?=$Nota->getConteudo();?></textarea>
            <br/>
            <input type="submit" value="Enviar"/>
        </form>
        <hr/>
        <?
    }
    
    public function excluirAction( $notaToken ){
        $Nota = new Nota($notaToken);
        $cadernoToken = $Nota->getCadernoToken();
        $Nota->delete();
        $this->redirect(url."/painel/caderno/visualizacao/{$cadernoToken}");
    }
}