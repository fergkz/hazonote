<?php

use Model\Usuario as Usuario;

class IndexController extends System\MyController
{
    private $Usuario = null;
    
    public function __construct()
    {
        parent::__construct();
        $this->Usuario = Usuario::getOnline();
    }
    
    public function indexAction()
    {
        $render['Usuario'] = $this->Usuario;
        $render['cadernos'] = $this->Usuario->listaCadernos();
//        debug($render);
        $this->view("index/index.twig")->display($render);
    }
    
}