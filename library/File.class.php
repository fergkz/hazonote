<?php

namespace Lib;

class File{

    public static function delete( $path )
    {
        if( is_file($path) ){
            chmod($path, 0777);
            return unlink($path);
        }else{
            $handle = opendir($path);
            while( $file = readdir($handle) ){
                if( $file != '..' && $file != '.' ){
                    if( is_file($path.DS.$file) ){
                        unlink($path.DS.$file);
                    }else{
                        self::delete($path.DS.$file);
                    }
                }
            }
            closedir($handle);
            chmod($path, 0777);
            return rmdir($path);
        }
    }
    
    public static function grantCascade( $path, $path_base, $mode )
    {
        $npath = str_replace("\\", "/", trim(str_replace($path_base, "", $path), "\\..\/"));
        
        $arr = explode("/", $npath);
        
//        debug($path_base,1);
//        debug($arr,1);
        
        foreach( $arr as $row ){
            $path_base .= DIRECTORY_SEPARATOR.(string)$row;
            chmod($path_base, $mode);
            debug($path_base,1);
        }
    }

}