<?php

namespace Core\System;

class Submodule{
    
    public $module;
    public $submodule;
    public $description;
    public $action;
    public $status;
    public $showMenu;
    public $grantRequire;
    public $helpers; # array(title, description)
    
    public function setModule( $module ){
        $this->module = $module;
    }

    public function getModule(){
        return $this->module;
    }

    public function setSubmodule( $submodule ){
        $this->submodule = $submodule;
    }

    public function getSubmodule(){
        return $this->submodule;
    }

    public function setDescription( $description ){
        $this->description = $description;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setAction( $action ){
        $this->action = $action;
    }

    public function getAction(){
        return $this->action;
    }

    public function setStatus( $status ){
        $this->status = $status;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setShowMenu( $showMenu ){
        $this->showMenu = $showMenu;
    }

    public function getShowMenu(){
        return $this->showMenu;
    }

    public function setGrantRequire( $grantRequire ){
        $this->grantRequire = $grantRequire;
    }

    public function getGrantRequire(){
        return $this->grantRequire;
    }
    
    public function isRestrict(){
        return $this->grantRequire == "Y" ? true : false;
    }
    
    public function setHelpers( $helpers ){
        $this->helpers = Functions::cryptSerialize($helpers);
    }

    public function getHelpers(){
        return Functions::decryptSerialize($this->helpers);
    }
    
}