<?php

namespace Core\System;

use Core\System\File as File;

class Module{
    
    public $filePath;
    public $status;
    
    public $mode;
    public $modeDescription;
    public $description;
    public $module;
    public $submodules;
    
    public $helpers; # array(title, description)
    
    public function setFilePath( $filePath ){
        $this->filePath = $filePath;
        return $this;
    }

    public function getFilePath(){
        return $this->filePath;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setMode( $mode ){
        $this->mode = $mode;
        return $this;
    }

    public function getMode(){
        return $this->mode;
    }

    public function setModeDescription( $modeDescription ){
        $this->modeDescription = $modeDescription;
        return $this;
    }

    public function getModeDescription(){
        return $this->modeDescription;
    }

    public function setDescription( $description ){
        $this->description = $description;
        return $this;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setModule( $module ){
        $this->module = $module;
        return $this;
    }

    public function getModule(){
        return $this->module;
    }

    public function setSubmodules( $submodules ){
        $this->submodules = $submodules;
        return $this;
    }

    public function getSubmodules($ind=null){
        if( $ind ){
            return $this->submodules[$ind];
        }else{
            return $this->submodules;
        }
    }
    
    public function setHelpers( $helpers ){
        $this->helpers = Functions::cryptSerialize($helpers);
        return $this;
    }

    public function getHelpers(){
        return Functions::decryptSerialize($this->helpers);
    }
    
    public function setOnlySubmodule( $module, $data ){
        $this->submodules[$module] = $data;
        return $this;
    }
    
    public static function exists(){
        $Module = new Module();
        $Module->setMode($GLOBALS['request']['mode']);
        $Module->setModule($GLOBALS['request']['module']);
        return $Module->load();
    }
    
    public function load(){
        if( $this->mode && $this->module ){
            $this->filePath = _getModePath($this->mode).ds.$this->module.".json";
        }else{
            return false;
        }
        if( !file_exists($this->filePath) ){
            return false;
        }
        $content = file_get_contents($this->filePath);
        if( !$content ){
            return false;
        }
        
        $this->loadByFileContent($content);
        
        return true;
    }
    
    public function loadByFileContent($content){
        $decode = JsonObject::jsonToObject($content);
        $array = get_object_vars($decode);
        foreach( $array as $ind => $row ){
            $this->$ind = $row;
        }
    }
    
    public static function getByFileContent($content){
        $Module = new Module();
        $Module->loadByFileContent($content);
        return $Module;
    }
    
    public function save(){
        if( !$this->description ){
            _setError("hz_module_description");
            return false;
        }
        if( !$this->mode || !_isMode($this->mode) ){
            _setError("hz_module_mode");
            return false;
        }
        if( !$this->module ){
            _setError("hz_module_module");
            return false;
        }
        $this->filePath = _getModePath($this->mode).ds.$this->module.".json";
        
        $content = JsonObject::objectToJson($this);
        
        if( !file_put_contents($this->filePath, $content) ){
            sleep(7);
            if( !file_put_contents($this->filePath, $content) ){
                _setError("hz_module_unable_xml");
                return false;
            }
        }
        return true;
    }

    public static function listAll(){
        foreach( $GLOBALS['config']['directories']['mode'] as $mode => $modeConfig ){
            if( $mode === "ERROR" ){ continue; }
            $File = new File(PATH."/".$modeConfig['path'], true);
            $files = $File->getFiles(".json", true);
            if( $files ){
                foreach( $files as $path ){
                    $Module = Module::getByFileContent(file_get_contents($path));
                    $modules[$mode][$Module->module] = $Module;
                }
            }else{
                $modules[$mode] = true;
            }
        }
        return $modules;
    }
    
}
