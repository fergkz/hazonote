<?php

namespace MVC;

use Core\Model\User as User;

_includeLib("Twig/Autoloader.php");

\Twig_Autoloader::register();

class Twig extends \Twig_Environment{

    public function __construct( $options = array() ){
        $directory = str_replace("\\", "/", _getModePath($GLOBALS['request']['mode']));

        $include[] = realpath("{$directory}/View");
        if( $GLOBALS['config']['template']['include_filepath'] ){
            foreach( $GLOBALS['config']['template']['include_filepath'] as $row ){
                $row = trim($row, "\\..\/");
                if( file_exists(PATH.DS.$row) ){
                    $include[] = realpath(PATH.DS.$row);
                }elseif( file_exists(PATH.$row) ){
                    $include[] = realpath(PATH.$row);
                }elseif( file_exists($row) ){
                    $include[] = realpath($row);
                }
            }
        }
        $include[] = PATH;
        
        $loader = new \Twig_Loader_Filesystem( $include );
        if( !empty($GLOBALS['config']['template']['compilation_cache']) 
                && $GLOBALS['config']['template']['compilation_cache']
                && empty($options['cache']) 
                && !empty($GLOBALS['config']['template']['cache_filepath'])
                && $GLOBALS['config']['template']['cache_filepath'] ){
            $cacheCompilePath = trim($GLOBALS['config']['template']['cache_filepath'], "\\..\/");
            $options['cache'] = path.ds.$cacheCompilePath.ds."twig_compilation";
            $options['auto_reload'] = true;
            if( !file_exists($options['cache']) ){
                mkdir($options['cache']);
            }
        }
        
        parent::__construct($loader, $options);

        if( $GLOBALS['config']['define'] ){
            foreach( $GLOBALS['config']['define'] as $ind => $val ){
                $this->addGlobal($ind, $val);
            }
        }
        $this->addGlobal("url", url);
        $this->addGlobal("path", path);
        $this->addGlobal("online", User::online());
        
        $this->addGlobal("globals", $GLOBALS);
        $this->addGlobal("get", $_GET);
        $this->addGlobal("post", $_POST);
        $this->addGlobal("files", $_FILES);
        $this->addGlobal("Online", User::online());

        $this->addGlobal("errors",  _getErrors());
        $this->addGlobal("success", _getSuccess());
        $this->addGlobal("alert",   _getAlert());
        $this->addGlobal("infos",   _getInfo());
        $this->addGlobal("is_ajax", @$_SERVER['HTTP_X_REQUESTED_WITH']);
        $this->addGlobal("ajax", @$_SERVER['HTTP_X_REQUESTED_WITH']);
        if( !isset($options['errors_ignore']) || !$options['errors_ignore'] ){
            _clearErrors();
            _clearSuccess();
            _clearAlert();
            _clearInfo();
            _clearScrollPrev();
        }

        $this->addExtension(new \Twig_Extension_StringLoader());
        
        $this->declareFunction('_isGranted');
        $this->declareFunction('debug');
        $this->declareFunction('print_r');
        $this->declareFunction('var_dump');
        $this->declareFunction('inArray');
        $this->declareFunction('_getModePath');
        $this->declareFunction('_getErrors');
        $this->declareFunction('_getAlert');
        
        if( !empty($GLOBALS['config']['template']['twig_functions']) ){
            foreach( $GLOBALS['config']['template']['twig_functions'] as $function ){
                $this->declareFunction($function);
            }
        }
        
    }
    
    public static function getInstance( $options = array() ){
        return new Twig($options);
    }
    
    public function declareFunction($function){
        $function = new \Twig_SimpleFunction($function,
            $function
        );
        $this->addFunction($function);
        return $this;
    }

}