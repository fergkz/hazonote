<?php

namespace MVC;

class Controller{

    public static function getInstance(){
        $classCalled = get_called_class();
        return new $classCalled();
    }

    public function __destruct(){
    }

    protected function view( $options = array() ){
        return new View($options);
    }
    
    protected function redirect( $url ){
        header("Location: {$url}");
        exit;
    }
    
}