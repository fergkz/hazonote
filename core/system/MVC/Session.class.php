<?php

namespace MVC;

class Session{
    
    public static function start(){
        if( !session_id() ){
            if( !empty($GLOBALS['config']['session']['session_cache_expire']) ){
                session_cache_expire($GLOBALS['config']['session']['session_cache_expire']);
            }
            if( !empty($GLOBALS['config']['session']['session_name']) ){
                session_name($GLOBALS['config']['session']['session_name']);
            }
            if( !empty($GLOBALS['config']['session']['full_session_save_path']) ){
                session_save_path($GLOBALS['config']['session']['full_session_save_path ']);
            }elseif( !empty($GLOBALS['config']['session']['session_save_path']) ){
                session_save_path(PATH.DS.trim($GLOBALS['config']['session']['session_save_path'], "\\..\/"));
            }
            session_start();
        }
    }

    public static function get( $index = null ){
        self::start();
        if( $index === null ){
            return $_SESSION;
        }else{
            return empty($_SESSION[$index]) ? null : unserialize($_SESSION[$index]);
        }
    }
    
    public static function set( $index, $value = null ){
        self::start();
        return $_SESSION[$index] = serialize($value);
    }
    
    public static function del( $index = null ){
        self::start();
        if( $index === null ){
            $_SESSION = null;
        }else{
            $_SESSION[$index] = null;
        }
    }
    
    public static function destroy(){
        self::del();
        if( session_id() ){
            session_destroy();
        }
        return true;
    }
    
}