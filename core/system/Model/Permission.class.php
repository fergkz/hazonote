<?php

namespace Core\Model;

use MVC\Model as Model;

class Permission extends Model{
    
    protected $ID;
    protected $mode;
    protected $module;
    protected $submodule;
    protected $groupID;
    protected $groupObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setMode( $mode ){
        $this->mode = $mode;
        return $this;
    }

    public function getMode(){
        return $this->mode;
    }

    public function setModule( $module ){
        $this->module = $module;
        return $this;
    }

    public function getModule(){
        return $this->module;
    }

    public function setSubmodule( $submodule ){
        $this->submodule = $submodule;
        return $this;
    }

    public function getSubmodule(){
        return $this->submodule;
    }

    public function setGroupID( $groupID ){
        $this->groupObj = Group::getByID($groupID);
        $this->groupID = $groupID;
        return $this;
    }

    public function getGroupID(){
        return $this->groupID;
    }

    public function setGroupObj( $groupObj ){
        if( is_object($groupObj) ){
            $this->groupID = $groupObj->getID();
        }
        $this->groupObj = $groupObj;
        return $this;
    }

    public function getGroupObj(){
        if( !$this->groupObj ){
            $this->groupObj = Group::getByID($this->groupID);
        }
        return $this->groupObj;
    }
    
    public function load(){
        if( $this->ID ){
            return parent::load();
        }
        $sql = "select id from hazo_permission 
                    where mode = :mode
                      and module = :module
                      and submodule = :submodule
                      and group_id = :group_id";
        $bind['mode']      = $this->mode;
        $bind['module']    = $this->module;
        $bind['submodule'] = $this->submodule;
        $bind['group_id']  = $this->groupID;
        $res = _query($sql, $bind);
        if( isset($res[0]['id']) && $res[0]['id'] ){
            $this->ID = $res[0]['id'];
            return $this;
        }
        return false;
    }
    
    protected function triggerBeforeDelete(){
        $sql = "select id from hazo_menu 
                    where mode = :mode
                      and module = :module
                      and submodule = :submodule
                      and group_id = :group_id ";
        $bind['mode']      = $this->old->mode;
        $bind['module']    = $this->old->module;
        $bind['submodule'] = $this->old->submodule;
        $bind['group_id']  = $this->old->groupID;
        $res = _query($sql, $bind);
        if( $res ){
            foreach( $res as $row ){
                Menu::deleteAll($row['menu']);
            }
        }
    }
    
    public static function groupIsGranted($groupID, $mode, $module, $submodule){
        $groupID = is_object($groupID) ? $groupID->getID() : $groupID;
        $Permission = new Permission();
        $Permission->setGroupID($groupID);
        $Permission->setMode($mode);
        $Permission->setModule($module);
        $Permission->setSubmodule($submodule);
        return $Permission->load();
    }
    
}