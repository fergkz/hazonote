<?php
function _getClassPath( $classe ){
    $classe = str_replace("\\", "/", $classe);
    foreach( $GLOBALS['config']['namespaces'] as $namespace => $path ){
        $namespace = str_replace("\\", "/", $namespace)."/";
        if( @substr($classe,0,strlen($namespace)) == $namespace ){
            $realpath = str_replace($namespace, $path, $classe);
            $realpath = PATH.DS.$realpath.".class.php";
            break;
        }
    }
    return isset($realpath) ? realpath($realpath) : $classe.".class.php";
}

function hazoAutoload( $classe ){
    $filepath = _getClassPath($classe);
    if( $filepath && file_exists($filepath) ){
        @include_once( $filepath );
    }else{
        @include_once( $filepath );
    }
}
spl_autoload_register("hazoAutoload");

function add_include_path($path){
    $nPath = get_include_path() . PATH_SEPARATOR . $path;
    set_include_path($nPath);
    return $nPath;
}
function _includeLib($lib){
    $libPath = PATH."/core/Library/{$lib}";
    if( file_exists($libPath) ){
        include_once($libPath);
        return true;
    }
    return false;
}