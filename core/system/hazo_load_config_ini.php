<?php

# Config core
if( file_exists(dirname(__DIR__).DS."config.ini") ){
    $configs = parse_ini_file(dirname(__DIR__).DS."config.ini", true);
    foreach( $configs as $ind1 => $row1 ){
        foreach( $row1 as $ind2 => $row2 ){
            $GLOBALS['config'][$ind1][$ind2] = $row2;
        }
    }
}

# Config fixed globals params
if( file_exists(dirname_client_config.DS."application.ini") ){
    $configs = parse_ini_file(dirname_client_config.DS."application.ini", true);
    foreach( $configs as $ind1 => $row1 ){
        foreach( $row1 as $ind2 => $row2 ){
            $GLOBALS['config'][$ind1][$ind2] = $row2;
        }
    }
}

# Config client
if( file_exists(dirname_client_config.DS."config.ini") ){
    $configs = parse_ini_file(dirname_client_config.DS."config.ini", true);
    foreach( $configs as $ind1 => $row1 ){
        foreach( $row1 as $ind2 => $row2 ){
            $GLOBALS['config'][$ind1][$ind2] = $row2;
        }
    }
}

if( isset($GLOBALS['config']['define']) && $GLOBALS['config']['define'] ){
    foreach( $GLOBALS['config']['define'] as $ind => $value ){
        if( !defined($ind) ){
            if( in_array($ind, array("url_server","url_folder")) ){
                define($ind, trim($value, "\\..\/"));
                $GLOBALS['config']['define'][$ind] = trim($value, "\\..\/");
            }elseif( in_array($ind, array("path_root")) ){
                define($ind, rtrim($value, "\\..\/"));
                $GLOBALS['config']['define'][$ind] = rtrim($value, "\\..\/");
            }else{
                define($ind, $value);
            }
        }
    }
}

if( isset($GLOBALS['config']['ini_set']) && $GLOBALS['config']['ini_set'] ){
    foreach( $GLOBALS['config']['ini_set'] as $ind => $value ){
        ini_set($ind, $value);
    }
}

# Define URL
if( !isset($GLOBALS['config']['define']['url_server']) || !$GLOBALS['config']['define']['url_server'] ){
    if( isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] ){
        $GLOBALS['config']['define']['url_server'] = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'];
    }else{
        $GLOBALS['config']['define']['url_server'] = "http://".$_SERVER['SERVER_NAME'];
    }
}
$GLOBALS['config']['define']['url_server'] = trim($GLOBALS['config']['define']['url_server'],"\\..\/");

if( !defined("URL") ){
    if( isset($GLOBALS['config']['define']['url_folder']) && trim($GLOBALS['config']['define']['url_folder'],"\\..\/") ){
        define("URL", $GLOBALS['config']['define']['url_server']."/".trim($GLOBALS['config']['define']['url_folder'],"\\..\/"));
    }else{
        define("URL", $GLOBALS['config']['define']['url_server']);
    }
}
if( !defined("url") ){
    define("url", URL);
}

# Define folder path
if( !isset($GLOBALS['config']['define']['path_root']) || !$GLOBALS['config']['define']['path_root'] ){
    $GLOBALS['config']['define']['path_root'] = trim($_SERVER['DOCUMENT_ROOT'],"\\..\/");
}

if( !defined("PATH") ){
    define("PATH", $GLOBALS['config']['define']['path_root']);
}
if( !defined("path") ){
    define("path", PATH);
}

$GLOBALS['config']['path'] = path;
$GLOBALS['config']['url']  = url;

if( @$GLOBALS['config']['ini_set']['date_timezone'] ){
    date_default_timezone_set($GLOBALS['config']['ini_set']['date_timezone']);
}

foreach( $GLOBALS['config']['directories'] as $ind => $value ){
    $dir = explode(".", trim($ind));
    $GLOBALS['config']['directories'][$dir[0]][$dir[1]][$dir[2]] = $value;
}

unset($configs);  unset($ind1);  unset($row1);  unset($ind2);
unset($row2);     unset($ind);   unset($value); unset($dir);