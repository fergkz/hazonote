<?php

function _coalesce( $variable = null, $value = null ){
    return isset($variable) && $variable ? $variable : $value;
}

function _stringToCamelMVC( $string ){
    return preg_replace_callback("(-.{1})", create_function('$a', 'return str_replace("-","",strtoupper($a[0]));'), $string);
}

function _getParamsForUrl( $url ){
    $urlRequisitado = str_replace(array( '/?', '?', '=', '&' ), "/", trim($url, "\\..\/"));
    $tmp = explode('/', trim($urlRequisitado));
    foreach( $tmp as $i => $r ){
        $new[] = $r;
    }
    $mode = _isMode(strtoupper($new[0])) ? strtoupper($new[0]) : "F";
    $cont = _isMode(strtoupper($new[0])) ? 1 : 0;
    
    if( @$new[$cont] == "$" ){
        $module = 'index';
    }else{
        $module = @_coalesce($new[$cont], "index");
        $cont++;
    }
    if( @$new[$cont] == "$" ){
        $submodule = "index";
    }else{
        $submodule = @_coalesce($new[$cont], "index");
        $cont++;
    }

    return array( "mode" => $mode, "module" => $module, "submodule" => $submodule, "cont" => $cont, "new" => $new );
}

function _includeError( $code ){
    switch( $code ){
        case 307:
            $msg = "Moved Permanently";
            break;
        case 404:
            $msg = "Page not found";
            break;
        case 403:
            $msg = "You not granted to access this page";
            break;
        case "sqlConnect":
            $msg = "Not connected to the database";
            break;
        default:
            $msg = "Page not found";
            break;
    }
    $method = "error{$code}";
    if( file_exists(_getModePath("ERROR")."/Controller/ErrorController.class.php") ){
        include_once(_getModePath("ERROR")."/Controller/ErrorController.class.php");
        if( class_exists("ErrorController") ){
            $Class = new ErrorController();
            $method = $method."Action";
            if( method_exists($Class, $method) ){
                $Class->$method();
                exit;
            }
        }
    }

    echo "<center><br/><br/>$msg</center>";
    exit;
}

function _isGranted( $url = null, $groupID = null ){
    if( !$url ){
        $url = $GLOBALS['url']['request'];
    }
    if( substr($url, 0, 1) === "/" ){
        $url = substr($url, 1);
    }

    $params = _getParamsForUrl($url);

    $mode = $params['mode'];
    $module = $params['module'];
    $submodule = $params['submodule'];

    $Module = new Core\System\Module();
    $Module->setMode($mode);
    $Module->setModule($module);
    $Module->load();
    $Submodule = &$Module->submodules[$submodule];

    $page = array(
        "Mode" => _getModeDescr($mode),
        "Module" => $Module,
        "Submodule" => $Submodule
    );

    if( !_isModeRestrict($mode) ){
        return $page;
    }

    $online = Core\Model\User::online();

    if( !$online ){
        return false;
    }

    if( $online->getGroupObj()->getMode() == "S" ){
        return $page;
    }

    $levelOnline = _getModeLevel($online->getGroupObj()->getMode());

    if( _getModeLevel($mode) > $levelOnline ){
        return false;
    }

    if( !$Module || $Module->status !== "O" || !$Submodule || $Submodule->status !== "O" ){
        return false;
    }

    if( $Submodule->grantRequire !== "Y" ){
        return $page;
    }

    if( Core\Model\Permission::groupIsGranted($online->getGroupID(), $mode, $module, $submodule) ){
        return $page;
    }else{
        return false;
    }
}

if( isset($GLOBALS['config']['define']['url_folder']) && $GLOBALS['config']['define']['url_folder'] ){
    $url['request'] = substr(trim($_SERVER['REQUEST_URI'], "\\..\/"), strlen($GLOBALS['config']['define']['url_folder']));
}else{
    $url['request'] = trim($_SERVER['REQUEST_URI'], "\\..\/");
}

$url['tmp'] = _getParamsForUrl($url['request']);

if( $url['tmp']['mode'] == "F" && $url['tmp']['module'] == "logout" ){
    Core\Model\User::logout();
    header("Location: ".url."");
    exit;
}

$GLOBALS['request']['mode'] = $url['tmp']['mode'];
$GLOBALS['request']['module'] = $url['tmp']['module'];
$GLOBALS['request']['submodule'] = $url['tmp']['submodule'];
$url['cont'] = $url['tmp']['cont'];
$url['new'] = $url['tmp']['new'];

$url['set_next_params'] = false;
for( $url['i'] = $url['cont']; $url['i'] < count($url['new']); $url['i']++ ){
    if( isset($url['new'][$url['i']]) ){
        if( $url['new'][$url['i']] == "$" ){
            $url['set_next_params'] = true;
            $url['i']++;
        }
        if( $url['set_next_params'] === false ){
            $url['indice'] = (String)$url['new'][$url['i']];
            $url['i']++;
            $_GET[$url['indice']] = isset($url['new'][$url['i']]) ? urldecode("{$url['new'][$url['i']]}") : null;
        }elseif( $url['set_next_params'] === true ){
            $GLOBALS['request']['url_params'][] = @$url['new'][$url['i']];
        }
    }
}

/* * *** AUTO-LOGIN in "__hazo__login__" **** */
if( $_POST && isset($_POST['__hazo__login__']) ){
    if( @!Core\Model\User::login($_POST['login'], $_POST['password']) ){
        _setError("hz_user_login_incorrect");
    }else{
        if( $GLOBALS['request']['mode'] == "F" ){
            if( Core\Model\User::online()->getGroupObj()->getDefaultUrl() ){
                header("Location: ".url.Core\Model\User::online()->getGroupObj()->getDefaultUrl());
            }else{
                header("Location: ".url."/".strtolower(Core\Model\User::online()->getGroupObj()->getMode()));
            }
            exit;
        }
    }
}

$GLOBALS['page'] = _isGranted();
if( !$GLOBALS['page'] ){
    _includeError(403);
}

$url['modePath'] = trim(str_replace(PATH, "", _getModePath($GLOBALS['request']['mode'])), "\\..\/");
$url['expl_mode_path'] = @explode("/", implode("/", explode("\\", $url['modePath'])));
$url['loaded'] = PATH;
foreach( $url['expl_mode_path'] as $url['tmp'] ){
    $url['loaded'] .= ds.$url['tmp'];
    if( file_exists($url['loaded'].ds."config.php") ){
        include_once($url['loaded'].ds."config.php");
    }
}

unset($url);
/* Treatment to mvc model */

$MVCCONTROLL['modePath'] = _getModePath($GLOBALS['request']['mode']);
$MVCCONTROLL['classControllerName'] = ucfirst(_stringToCamelMVC($GLOBALS['request']['module']))."Controller";
$MVCCONTROLL['classControlerFileName'] = "{$MVCCONTROLL['modePath']}/Controller/{$MVCCONTROLL['classControllerName']}.class.php";

if( !defined("MODE") ){
    define('MODE', $GLOBALS['request']['mode']);
    define('mode', MODE);
    define('URL_MODE', URL."/".strtolower(MODE));
    define('url_mode', URL_MODE);
}

if( !defined("MODULE") ){
    define('MODULE', $GLOBALS['request']['module']);
    define('module', MODULE);
    define('URL_MODULE', URL_MODE."/".MODULE);
    define('url_module', URL_MODULE);
}

if( !defined("SUBMODULE") ){
    define('SUBMODULE', $GLOBALS['request']['submodule']);
    define('submodule', SUBMODULE);
    define('URL_SUBMODULE', URL_MODULE."/".SUBMODULE);
    define('url_submodule', URL_SUBMODULE);
}

if( !defined("PATH_MODE") ){
    define('PATH_MODE', PATH.DS.$GLOBALS['request']['mode']);
    define('path_mode', PATH_MODE);
}

add_include_path(str_replace(array( "\\", "/" ), DS, _getModePath(MODE).DS."Controller"));

if( file_exists($MVCCONTROLL['classControlerFileName']) ){
    include_once($MVCCONTROLL['classControlerFileName']);
    if( class_exists($MVCCONTROLL['classControllerName']) ){
        $MVCCONTROLL['Controller'] = new $MVCCONTROLL['classControllerName']();

        $MVCCONTROLL['methodController'] = _stringToCamelMVC($GLOBALS['request']['submodule'])."Action";

        if( method_exists($MVCCONTROLL['Controller'], $MVCCONTROLL['methodController']) ){
            if( @$GLOBALS['request']['url_params'] && count($GLOBALS['request']['url_params']) > 0 ){
                $MVCCONTROLL['returnController'] = call_user_func_array(array( $MVCCONTROLL['Controller'], $MVCCONTROLL['methodController'] ), $GLOBALS['request']['url_params']);
            }else{
                $MVCCONTROLL['returnController'] = $MVCCONTROLL['Controller']->$MVCCONTROLL['methodController']();
            }
        }elseif( !$GLOBALS['request']['submodule'] && method_exists($MVCCONTROLL['Controller'], "indexAction") ){
            if( @$GLOBALS['request']['url_params'] && count($GLOBALS['request']['url_params']) > 0 ){
                $MVCCONTROLL['returnController'] = call_user_func_array(array( $MVCCONTROLL['Controller'], "indexAction" ), $GLOBALS['request']['url_params']);
            }else{
                $MVCCONTROLL['returnController'] = $MVCCONTROLL['Controller']->indexAction();
            }
        }else{
            $MVCCONTROLL['returnController'] = 404;
        }
        if( $MVCCONTROLL['returnController'] === 404 || $MVCCONTROLL['returnController'] === false ){
            _includeError(404);
        }elseif( $MVCCONTROLL['returnController'] && is_numeric($MVCCONTROLL['returnController']) ){
            _includeError($MVCCONTROLL['returnController']);
        }
    }else{
        _includeError(404);
    }
}else{
    _includeError(404);
}