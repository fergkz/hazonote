<?php

use MVC\Controller as Controller;
use MVC\Twig as Twig;
use Core\System\Module as Module;
use Core\Model\Permission as Permission;
use Core\Model\Group as Group;

class PermissionsController extends Controller{
    
    public function listAction(){
        $Group = Group::getByID($_GET['group']);
        $modes = Module::listAll();
        foreach( $modes as $mode => $modules ){
            if( _getModeLevel($mode) > _getModeLevel($Group->getMode()) || !is_array($modules) ){
                continue;
            }
            $data[$mode] = $modules;
        }
        $render['Group'] = $Group;
        $render['modes'] = @$data;
        $Twig = new Twig();
        $Twig->declareFunction("_getModeDescr");
        $Twig->declareFunction("_getSubModuleActionDescr");
        $Twig->declareFunction("_getModuleSitDescr");
        function PermissionGroupIsGranted($groupID, $mode, $module, $submodule){
            return Permission::groupIsGranted($groupID, $mode, $module, $submodule);
        }
        $Twig->declareFunction("PermissionGroupIsGranted");
        $Template = $Twig->loadTemplate("permissions/list.html");
        echo $Template->render($render);
    }
    
    public function changeAction( $group, $mode, $module, $submodule, $default = null ){
        $Permission = new Permission();
        $Permission->setGroupID($group);
        $Permission->setMode($mode);
        $Permission->setModule($module);
        $Permission->setSubmodule($submodule);
        if( $Permission->load() ){
            if( $Permission->save("D") ){
                _commit();
                $data['status']  = "N";
                $data['default'] = false;
            }
        }else{
            if( $Permission->save("I") ){
                _commit();
                $data['status']  = "Y";
                $data['default'] = true;
            }
        }
        echo json_encode($data);
    }
    
    public function changeDefaultAction( $group, $mode, $module, $submodule ){
        $Group = Group::getByID($group);
        
        $default = "{$mode}|{$module}|{$submodule}";
        
        if( $Group->getDefaultPage() == $default ){
            $Group->setDefaultPage(null);
            $Group->save();
            $data['status'] = "N";
        }else{
            $Group->setDefaultPage($default);
            $Group->save();
            $data['status'] = "Y";
        }
        
        echo json_encode($data);
    }

}