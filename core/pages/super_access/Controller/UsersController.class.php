<?php

use MVC\Controller as Controller;
use MVC\Twig as Twig;
use Core\Model\User as User;
use Core\Model\Group as Group;

class UsersController extends Controller{
    
    public function indexAction(){
        $render['groups'] = Group::listAll();
        $Twig = new Twig();
        $Template = $Twig->loadTemplate("users/index.html");
        echo $Template->render($render);
    }
    
    public function changeAction(){
        $Group = @$_GET['group'] ? Group::getByID($_GET['group']) : new Group();
        $User = @$_GET['id'] ? User::getByID($_GET['id']) : new User();
        if( $_POST ){
            $User->setGroupID($_GET['group']);
            $User->setStatus($_POST['status']);
            $User->setName($_POST['name']);
            $User->setLogin($_POST['login']);
            if( $_POST['password'] ){
                $User->setPassword($_POST['password']);
            }
            $User->setEmail($_POST['email']);
            
            if( $_FILES['image'] && $_FILES['image']['name'] ){
                $User->setImageContent(file_get_contents($_FILES['image']['tmp_name']));
                $User->setImageSize($_FILES['image']['size']);
                $User->setImageName($_FILES['image']['name']);
                $User->setImageType($_FILES['image']['type']);
            }elseif( $_POST['delete_image'] == 'S' ){
                $User->setImageContent(null);
                $User->setImageSize(null);
                $User->setImageName(null);
                $User->setImageType(null);
                $User->setImageFilename(null);
            }
            
            if( @!$_GET['id'] && !$_POST['password'] ){
                _setError("hz_hazo_user_pass_empty");
            }
            elseif( $_POST['password'] !== $_POST['rpassword'] ){
                _setError("hz_hazo_user_pass_match");
            }
            elseif( $User->save() ){
                _commit();
                $this->redirect(url."/s/users");
            }
        }
        $Twig = new Twig();
        $render['Group'] = $Group;
        $render['User'] = $User;
        $Template = $Twig->loadTemplate("users/change.html");
        echo $Template->render($render);
    }
    
    public function listGroupAction(){
        $render['users'] = User::listByGroup($_GET['group']);
        $Twig = new Twig();
        $Template = $Twig->loadTemplate("users/list-group.html");
        echo $Template->render($render);
    }

}