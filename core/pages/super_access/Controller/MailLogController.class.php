<?php

use MVC\Controller as Controller;

class MailLogController extends Controller{
    
    public function indexAction(){
        $json = @json_decode(file_get_contents(path.ds.$GLOBALS['config']['email']['send_to_file'])) ?: null;
        $render = array();
        if( $json ){
            
            foreach( $json as $row ){
                if( is_object($row) ){
                    $render['content'][] = array(
                        'from'    => $row->from,
                        'body'    => $row->body,
                        'subject' => $row->subject,
                        'to'      => $row->to,
                        'date'     => $row->date
                    );
                }else{
                    $render['content'][]['body'] = $row;
                }
            }
            
        }
        $this->view()->display($render);
    }
    
    public function clearAction(){
        file_put_contents(path.ds.$GLOBALS['config']['email']['send_to_file'], "");
        $this->redirect(url."/s/mail-log");
    }
    
}
