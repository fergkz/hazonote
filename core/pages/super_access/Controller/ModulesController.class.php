<?php

use MVC\Controller as Controller;
use MVC\Twig as Twig;
use Core\System\Module as Module;
use Core\System\Submodule as Submodule;

class ModulesController extends Controller{
    
    public function indexAction(){
        $render['modes'] = Module::listAll();
        $Twig = new Twig();
        $Twig->declareFunction("_getModeDescr");
        $Twig->declareFunction("_getSubModuleActionDescr");
        $Twig->declareFunction("_getModuleSitDescr");
        $Template = $Twig->loadTemplate("modules/index.html");
        echo $Template->render($render);
    }
    
    public function changeAction(){
        $Module = new Module();
        if( $_GET['mode'] ){
            $Module->setMode($_GET['mode']);
            if( @$_GET['module'] ){
                $Module->setModule($_GET['module']);
                $Module->load();
            }
        }
        if( $_POST ){
            $Module->status = $_POST['mod-status'];
            $Module->description = $_POST['mod-description'];
            $Module->module = $_POST['mod-module'];
            $Module->submodules = array();
            
            if( @$_POST['hid-count'] ){
                foreach( $_POST['hid-count'] as $cont => $val ){
                    if( count($_POST['hid-count']) < 1 ){ break; }
                    if( $val == 'N' ){ continue; }
                    if( !@$_POST['sub-submodule'][$cont] &&
                        !@$_POST['sub-action'][$cont] &&
                        !@$_POST['sub-description'][$cont] &&
                        @$_POST['sub-grant-require'][$cont] != "Y" &&
                        @$_POST['sub-show-menu'][$cont] != "Y" &&
                        !@$_POST['sub-status'][$cont] ){
                        continue;
                    }

                    if( !@$_POST['sub-submodule'][$cont] ){
                        $error["Submodule is empty"] = true;
                    }
                    if( !@$_POST['sub-action'][$cont] ){
                        $error["Action is empty"] = true;
                    }
                    if( !@$_POST['sub-description'][$cont] ){
                        $error["Description is empty"] = true;
                    }
                    if( !@$_POST['sub-status'][$cont] ){
                        $error["Status is empty"] = true;
                    }

                    $Submodule = new Submodule();
                    $Submodule->setSubmodule(@$_POST['sub-submodule'][$cont]);
                    $Submodule->setAction(@$_POST['sub-action'][$cont]);
                    $Submodule->setDescription(@$_POST['sub-description'][$cont]);
                    $Submodule->setGrantRequire(@$_POST['sub-grant-require'][$cont]);
                    $Submodule->setModule($Module->module);
                    $Submodule->setShowMenu(@$_POST['sub-show-menu'][$cont]);
                    $Submodule->setStatus(@$_POST['sub-status'][$cont]);
                    $Module->submodules[$Submodule->getSubmodule()] = $Submodule;
                }
            }
            if( @$error ){
                foreach( $error as $ind => $row ){
                    _setError($ind);
                }
            }elseif( !@$_GET['module'] ){
                if( $Module->save() ){
                    header("Location: ".url."/s/modules/change?mode={$_GET['mode']}&module={$Module->module}");
                    exit;
                }
            }else{
                if( $Module->save() ){
                    header("Location: ".url."/s/modules");
                    exit;
                }
            }
        }
        $render['Module'] = $Module;
        $Twig = new Twig();
        $Twig->declareFunction("_getModeDescr");
        $Template = $Twig->loadTemplate("modules/change.html");
        echo $Template->render($render);
    }
    
    public function helpersAction( $mode, $module ){
        $Module = new Module();
        $Module->setMode($mode);
        $Module->setModule($module);
        $Module->load();
        
        if( $_POST ){
            $return = $this->changeHelpers($Module);
            if( $return ){
                $Module = $return;
                _setSuccess("hz_file_saved");
            }
        }
        
        $render['Module'] = $Module;
        $render['mode_description'] = _getModeDescr($mode);
        
        $this->view()->display($render);
    }
    
    private function changeHelpers( $Module ){
        if( count($_POST['mode_title']) > 0 ){
            $helpers = array();
            foreach( $_POST['mode_title'] as $ind => $title ){
                if( !$title ){
                    continue;
                }
                $helpers[] = array(
                    "title" => $title,
                    "description" => $_POST['mode_description'][$ind]
                );
            }
            $Module->setHelpers($helpers);
        }
        foreach( $Module->getSubmodules() as $sub => $Submodule ){
            if( count($_POST['title'][$Submodule->getSubmodule()]) > 0 ){
                $helpers = array();
                foreach( $_POST['title'][$Submodule->getSubmodule()] as $ind => $title ){
                    if( !$title ){
                        continue;
                    }
                    $helpers[] = array(
                        "title" => $title,
                        "description" => $_POST['description'][$Submodule->getSubmodule()][$ind]
                    );
                }
                $Submodule->setHelpers($helpers);
            }
            $submodules[$Submodule->getSubmodule()] = $Submodule;
        }
        $Module->setSubmodules($submodules);

        if( $Module->save() ){
            return $Module;
        }else{
            return false;
        }
    }
    
}